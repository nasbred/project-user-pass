package ru.kharitonova.graphql;

import io.smallrye.common.annotation.NonBlocking;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;
import ru.kharitonova.entity.PassportEntity;
import ru.kharitonova.entity.UserEntity;
import ru.kharitonova.repository.PassportRepository;
import ru.kharitonova.repository.UserRepository;
import ru.kharitonova.service.PassportService;

import javax.inject.Inject;

@GraphQLApi
public class UserResource {

    @Inject
    UserRepository userRepository;

    @Inject
    PassportRepository passportRepository;

    @Query
    @Description("Get user by userId")
    @NonBlocking
    public Uni<UserEntity> getUser(@Name("userId") long id) {
        return userRepository.findByUserId(id);
    }

    @Query
    @Description("Get passport by passportId")
    @NonBlocking
    public Uni<PassportEntity> getPassport(@Name("passportId") long id) {
        return passportRepository.findByPasswordId(id);
    }

    @Query
    @Description("Get passport by userId")
    @NonBlocking
    public Uni<PassportEntity> getPassportByUser(@Name("userId") long id) {
        return passportRepository.findByUserId(id);
    }

}
