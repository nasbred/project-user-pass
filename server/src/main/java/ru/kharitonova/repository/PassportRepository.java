package ru.kharitonova.repository;

import io.smallrye.mutiny.Uni;
import lombok.RequiredArgsConstructor;
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.kharitonova.entity.PassportEntity;
import ru.kharitonova.entity.UserEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PassportRepository {
    private static final String SELECT_WHERE_PASSWORD_ID = "SELECT u FROM PassportEntity u WHERE u.id = ";
    private static final String SELECT_WHERE_USER_ID = "SELECT u FROM PassportEntity u WHERE u.valid = 'Y' AND u.user_id = ";
    private static final String DELETE_WHERE_PASSWORD_ID = "DELETE FROM PassportEntity u WHERE u.id = ";
    private static final String DELETE_WHERE_USER_ID = "DELETE FROM PassportEntity u WHERE u.user_id = ";

    @Inject
    private final SessionFactory factory;

    public PassportRepository(SessionFactory factory) {
        this.factory = factory;
    }

    public Uni<PassportEntity> save(@NotNull PassportEntity passportEntity) {
        return findByPasswordId(passportEntity.getId())
                .onItem().transformToUni(passport -> {
                    passport.setUser_id(passportEntity.getUser_id());
                    passport.setPassword_num(passportEntity.getPassword_num());
                    passport.setValid(passportEntity.getValid());
                    passport.setCreated(passportEntity.getCreated());
                    return factory.withTransaction(
                            session -> session.merge(passport));
                })
                .onFailure().recoverWithUni(() -> {
                    final PassportEntity passport = new PassportEntity();
                    passport.setId(passportEntity.getId());
                    passport.setUser_id(passportEntity.getUser_id());
                    passport.setPassword_num(passportEntity.getPassword_num());
                    passport.setValid(passportEntity.getValid());
                    passport.setCreated(passportEntity.getCreated());
                    return factory.withTransaction(
                            session -> session.merge(passport)
                    );
                }).flatMap(u -> Uni.createFrom().item(passportEntity));
    }

    public Uni<PassportEntity> findByPasswordId(@NotNull Long id) {
        return factory.withTransaction(
                session -> {
                    final String sql = SELECT_WHERE_PASSWORD_ID + id;
                    return session.<PassportEntity>createQuery(sql).getSingleResult()
                            .onFailure().recoverWithNull();
                }
        );
    }

    public Uni<PassportEntity> findByUserId(@NotNull Long id) {
        return factory.withTransaction(
                session -> {
                    final String sql = SELECT_WHERE_USER_ID + id;
                    return session.<PassportEntity>createQuery(sql).getSingleResult()
                            .onFailure().recoverWithNull();
                }
        );
    }

    public Uni<Void> removeByUserId(@NotNull Long id) {
        return factory.withTransaction(
                session -> {
                    final String sql = DELETE_WHERE_USER_ID + id;
                    return session.<UserEntity>createQuery(sql).executeUpdate()
                            .onFailure()
                            .invoke(e -> {
                                throw new RuntimeException("Error in removeByUserId :" +
                                        " cause : " + e.getCause() +
                                        " message : " + e.getMessage());
                            });
                }
        ).replaceWithVoid();
    }

    public Uni<Void> removeByPasswordId(@NotNull Long id) {
        return factory.withTransaction(
                session -> {
                    final String sql = DELETE_WHERE_PASSWORD_ID + id;
                    return session.<UserEntity>createQuery(sql).executeUpdate()
                            .onFailure()
                            .invoke(e -> {
                                throw new RuntimeException("Error in removeByPasswordId :" +
                                        " cause : " + e.getCause() +
                                        " message : " + e.getMessage());
                            });
                }
        ).replaceWithVoid();
    }

}