package ru.kharitonova.wrapper;

import ru.kharitonova.entity.PassportEntity;

import java.util.List;

public class PasswordTypesafeConnection {

    private List<PassportEntity> passwords;

    public List<PassportEntity> getUsers() {
        return passwords;
    }

    public void setUsers(List<PassportEntity> passwords) {
        this.passwords = passwords;
    }

}
