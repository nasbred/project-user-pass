package ru.kharitonova.kafka;

import java.time.Duration;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Outgoing;

import io.smallrye.mutiny.Multi;
import io.smallrye.reactive.messaging.kafka.Record;
import ru.kharitonova.entity.Tick;

@ApplicationScoped
public class TickKafkaGenerator {

    @Outgoing("ticks")
    public Multi<Record<String, Tick>> generate() {
        return Multi.createFrom().ticks().every(Duration.ofDays(1))
                .onOverflow().drop()
                .map(tick -> {
                    Tick message = new Tick();
                    return Record.of("key", message);
                });
    }

}