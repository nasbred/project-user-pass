package ru.kharitonova.repository;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.hibernate.reactive.mutiny.Mutiny.SessionFactory;
import ru.kharitonova.entity.UserEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class UserRepository {

    private static final String SELECT_ALL = "SELECT u FROM UserEntity u ORDER BY created ASC";
    private static final String SELECT_WHERE_USER_ID = "SELECT u FROM UserEntity u WHERE u.id = ";
    private static final String DELETE_WHERE_USER_ID = "DELETE FROM UserEntity u WHERE u.id = ";

    @Inject
    private final SessionFactory factory;

    public UserRepository(SessionFactory factory) {
        this.factory = factory;
    }

    public Uni<UserEntity> save(@NotNull UserEntity userEntity) {
        return findByUserId(userEntity.getId())
                .onItem().transformToUni(user -> {
                    user.setFst_name(userEntity.getFst_name());
                    user.setLst_name(userEntity.getLst_name());
                    user.setMid_name(userEntity.getMid_name());
                    user.setCreated(userEntity.getCreated());
                    return factory.withTransaction(
                            session -> session.merge(user));
                })
                .onFailure().recoverWithUni(() -> {
                    final UserEntity user = new UserEntity();
                    user.setId(userEntity.getId());
                    user.setFst_name(userEntity.getFst_name());
                    user.setLst_name(userEntity.getLst_name());
                    user.setMid_name(userEntity.getMid_name());
                    user.setCreated(userEntity.getCreated());
                    return factory.withTransaction(
                            session -> session.merge(user)
                    );
                }).flatMap(u -> Uni.createFrom().item(userEntity));
    }

    public Uni<UserEntity> findByUserId(@NotNull Long userId) {
        return factory.withTransaction(
                session -> {
                    final String sql = SELECT_WHERE_USER_ID + userId;
                    return session.<UserEntity>createQuery(sql).getSingleResult()
                            .onFailure().recoverWithNull();
                }
        );
    }

    public Uni<Void> removeByUserId(@NotNull Long userId) {
        return factory.withTransaction(
                session -> {
                    final String sql = DELETE_WHERE_USER_ID + userId;
                    return session.<UserEntity>createQuery(sql).executeUpdate()
                            .onFailure()
                            .invoke(e -> {
                                throw new RuntimeException("Error in removeByUserId :" +
                                        " cause : " + e.getCause() +
                                        " message : " + e.getMessage());
                            });
                }
        ).replaceWithVoid();
    }

}