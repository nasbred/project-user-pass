package ru.kharitonova.entity;

import java.time.LocalDateTime;

public class Tick {

    public boolean check;

    public LocalDateTime dateTime;

    public Tick() {
        this.check = true;
        this.dateTime = LocalDateTime.now();
    }

}
