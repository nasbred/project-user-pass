package ru.kharitonova.service;

import io.smallrye.mutiny.Uni;
import ru.kharitonova.entity.UserEntity;
import ru.kharitonova.repository.UserRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/user")
public class UserService {

    @Inject
    UserRepository repository;

    @GET
    @Path("/{id}")
    public Uni<UserEntity> getUserById(Long id) {
        return repository.findByUserId(id);
    }

    @GET
    @Path("/remove/{id}")
    public Uni<Void> removeUserById(Long id) {
        return repository.removeByUserId(id);
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Uni<UserEntity> save(UserEntity userEntity) {
        return repository.save(userEntity);
    }

}