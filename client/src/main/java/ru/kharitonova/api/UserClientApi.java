package ru.kharitonova.api;

import io.smallrye.graphql.client.typesafe.api.GraphQLClientApi;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.graphql.Name;
import ru.kharitonova.entity.PassportEntity;
import ru.kharitonova.entity.UserEntity;

@GraphQLClientApi(configKey = "users-typesafe")
public interface UserClientApi {

    Uni<UserEntity> getUser(@Name("userId") long id);
    Uni<PassportEntity> getPassport(@Name("passportId") long id);
    Uni<PassportEntity> getPassportByUser(@Name("userId") long id);

}
