package ru.kharitonova.kafka;

import io.smallrye.mutiny.Multi;
import org.eclipse.microprofile.reactive.messaging.Channel;
import ru.kharitonova.entity.Tick;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ticks")
public class TickResource {

    @Channel("ticks")
    Multi<Tick> ticks;

    @GET
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public Multi<Tick> stream() {
        return ticks;
    }

}
