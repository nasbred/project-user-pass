package ru.kharitonova.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "t1_user")
public class UserEntity {

    @Id
    public long id;

    @Column
    public LocalDateTime created;

    @Column(length = 50)
    public String fst_name;

    @Column(length = 50)
    public String mid_name;

    @Column(length = 50)
    public String lst_name;

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", created=" + created +
                ", fst_name='" + fst_name + '\'' +
                ", mid_name='" + mid_name + '\'' +
                ", lst_name='" + lst_name + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getFst_name() {
        return fst_name;
    }

    public void setFst_name(String fst_name) {
        this.fst_name = fst_name;
    }

    public String getMid_name() {
        return mid_name;
    }

    public void setMid_name(String mid_name) {
        this.mid_name = mid_name;
    }

    public String getLst_name() {
        return lst_name;
    }

    public void setLst_name(String lst_name) {
        this.lst_name = lst_name;
    }

    public UserEntity() {
    }

    public UserEntity(
            long id,
            LocalDateTime created,
            String fst_name,
            String mid_name,
            String lst_name
    ) {
        this.id = id;
        this.created = created;
        this.fst_name = fst_name;
        this.mid_name = mid_name;
        this.lst_name = lst_name;
    }
}
