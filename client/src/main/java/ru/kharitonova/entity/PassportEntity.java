package ru.kharitonova.entity;

import java.time.LocalDateTime;

public class PassportEntity {

    private long id;

    private LocalDateTime created;

    private long user_id;

    private String password_num;

    private String valid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getPassword_num() {
        return password_num;
    }

    public void setPassword_num(String password_num) {
        this.password_num = password_num;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "PassportEntity{" +
                "id=" + id +
                ", created=" + created +
                ", user_id='" + user_id + '\'' +
                ", password_num='" + password_num + '\'' +
                ", valid='" + valid + '\'' +
                '}';
    }

}
