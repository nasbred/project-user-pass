//package ru.kharitonova.service;
//
//import io.smallrye.common.annotation.Blocking;
//import io.smallrye.graphql.client.GraphQLClient;
//import io.smallrye.graphql.client.Response;
//import io.smallrye.graphql.client.core.Document;
//import io.smallrye.graphql.client.dynamic.api.DynamicGraphQLClient;
//import io.smallrye.mutiny.Uni;
//import ru.kharitonova.entity.UserEntity;
//import ru.kharitonova.wrapper.UserTypesafeConnection;
//import ru.kharitonova.wrapper.example.FilmTypesafeConnection;
//
//import javax.inject.Inject;
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import java.util.List;
//
//import static io.smallrye.graphql.client.core.Document.document;
//import static io.smallrye.graphql.client.core.Field.field;
//import static io.smallrye.graphql.client.core.Operation.operation;
//
//@Path("/users")
//public class UserResourceDynamic {
//    @Inject
//    @GraphQLClient("users-dynamic")
//    DynamicGraphQLClient dynamicClient;
//
//    @GET
//    @Path("/dynamic")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Blocking
//    public Uni<UserEntity> getUser(long id) throws Exception {
//        Document query = document(
//                operation(
//                    field("user",
//                        field("id"),
//                        field("fst_name"),
//                        field("mid_name"),
//                        field("lst_name")
//                    )
//                )
//        );
//        Response response = dynamicClient.executeSync(query);
//        return response.getObject(UserTypesafeConnection.class, "user").getUser(id);
//    }
//
//}